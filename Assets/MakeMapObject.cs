﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Xml;

public class MakeMapObject : MonoBehaviour {

	public Image icon;

	void Start () {
		MapScript.RegisterObject (gameObject, icon);
		print (MapScript.mapObjects.Count);
	}

	void OnDestroy () {
		print ("before " + MapScript.mapObjects.Count);
		MapScript.RemoveObject (gameObject);
		print (gameObject.name + " destroyed");
		print ("after " + MapScript.mapObjects.Count);
	}

	void OnCollisionEnter (Collision other) {
		if (other.gameObject.CompareTag("Player")) {
			Destroy (gameObject);
		}
	}
}
