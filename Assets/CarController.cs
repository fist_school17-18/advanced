﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CarController : MonoBehaviour {

    // Use this for initialization

    public float maxAngle = 30;
    public float maxTorque = 25;
    public float maxBrake = 50;

    public List<WheelCollider> frontWheelColliders;
    public List<WheelCollider> rearWheelColliders;

    public List<Transform> frontWheelMeshes;
    public List<Transform> rearWheelMeshes;

	public List<Transform> LeftTurnLights;
	public List<Transform> RightTurnLights;
	public List<Transform> BrakeLights;

	void FixedUpdate () {

        float angle = maxAngle * Input.GetAxis("Horizontal");
        float torque = maxTorque * Input.GetAxis("Vertical");

        foreach (WheelCollider wc in frontWheelColliders)
        {
            wc.steerAngle = angle;

            Vector3 position;
            Quaternion rotation;
            wc.GetWorldPose(out position, out rotation);

            frontWheelMeshes[frontWheelColliders.IndexOf(wc)].position = position;
            frontWheelMeshes[frontWheelColliders.IndexOf(wc)].rotation = rotation;

            wc.motorTorque = torque;
            wc.brakeTorque = 0;
        }	
        foreach (WheelCollider wc in rearWheelColliders)
        {
            Vector3 position;
            Quaternion rotation;
            wc.GetWorldPose(out position, out rotation);

            rearWheelMeshes[rearWheelColliders.IndexOf(wc)].position = position;
            rearWheelMeshes[rearWheelColliders.IndexOf(wc)].rotation = rotation;
        }

		if (Input.GetKeyDown (KeyCode.LeftArrow)) {
			foreach (Transform t in LeftTurnLights)
				t.GetComponentInChildren<Animator> ().SetBool("isBlink", true);
			foreach (Transform t in RightTurnLights)
				t.GetComponentInChildren<Animator> ().SetBool("isBlink", false);
		}

		if (Input.GetKeyDown (KeyCode.RightArrow)) {
			foreach (Transform t in LeftTurnLights)
				t.GetComponentInChildren<Animator> ().SetBool("isBlink", false);
			foreach (Transform t in RightTurnLights)
				t.GetComponentInChildren<Animator> ().SetBool("isBlink", true);
		}


		if (Input.GetKeyUp (KeyCode.LeftArrow)) {
			foreach (Transform t in LeftTurnLights)
				t.GetComponentInChildren<Animator> ().SetBool("isBlink", false);
		}

		if (Input.GetKeyUp (KeyCode.RightArrow)) {
			foreach (Transform t in RightTurnLights)
				t.GetComponentInChildren<Animator> ().SetBool("isBlink", false);
		}


		foreach (Transform t in BrakeLights)
			t.GetComponentInChildren<Light> ().enabled = torque < 0;
	}
}
