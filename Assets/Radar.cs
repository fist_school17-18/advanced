﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Radar : MonoBehaviour {

	public Transform root;
	public Transform prefab;
	public Transform player;
	public Transform panel;

	List<Marker> markers = new List<Marker>();

	// Use this for initialization
	void Start () {
		foreach (Transform t in root) {
			Transform tmp = Instantiate (prefab, Vector3.zero, Quaternion.identity, panel);
			Marker m = new Marker { obj = t, img = tmp };
			markers.Add (m);
		}
	}
	
	// Update is called once per frame
	void Update () {
		foreach (Marker m in markers) {
			Vector3 pos = m.obj.position - player.position;
			pos *= 10.0f;

			//float distance = pos.magnitude;
			//float deltaY = Mathf.Atan2 (pos.x, pos.z) * Mathf.Rad2Deg - 270 - player.eulerAngles.y;
			//pos.x = distance * Mathf.Cos (deltaY * Mathf.Deg2Rad) * -1;
			//pos.z = distance * Mathf.Sin (deltaY * Mathf.Deg2Rad);

			m.img.localPosition = new Vector3 (pos.x, pos.z, 0);
		}
		/*
		Quaternion rot = Quaternion.identity;
		rot.z = player.rotation.y;
		panel.rotation = rot;
		*/

		Vector3 tmp = Vector3.zero;
		tmp.z = player.eulerAngles.y;
		panel.rotation = Quaternion.Euler (tmp);

	}
}

class Marker {
	public Transform obj;
	public Transform img;
}
