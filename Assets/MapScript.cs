﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Linq;

public class MapObject {
	public MapObject (GameObject go, Image img) {
		this.icon = img;
		this.owner = go;
	}
	public Image icon { get; set; }
	public GameObject owner { get; set; } 
}

public class MapScript : MonoBehaviour {
	public Camera mapCamera;
	public Transform playerPos;
	public static List<MapObject> mapObjects = new List<MapObject> ();

	public static void RegisterObject(GameObject g, Image i) {
		Image img = Instantiate (i);
		mapObjects.Add (new MapObject (g, img));
	}

	public static void RemoveObject(GameObject o) {

		MapObject tmp = mapObjects.First (p => p.owner == o);
		Destroy (tmp.icon);
		mapObjects.Remove (tmp);

	}
	void DrawIcons () {
		foreach (MapObject mo in mapObjects) {
			Vector2 mop = new Vector2 (mo.owner.transform.position.x, mo.owner.transform.position.y);
			Vector2 pp = new Vector2 (playerPos.position.x, playerPos.position.y);

			if (Vector2.Distance (mop, pp) > 50) {
				mo.icon.enabled = false;
				continue;
			} else {
				mo.icon.enabled = true;
			}

			Vector3 screenPos = mapCamera.WorldToViewportPoint (mo.owner.transform.position);
			mo.icon.transform.SetParent (transform);
			print (transform.name);
			RectTransform rt = GetComponent<RectTransform> ();
			Vector3[] corners = new Vector3[4];
			rt.GetWorldCorners (corners);

			screenPos.x = Mathf.Clamp (screenPos.x * rt.rect.width + corners [0].x, corners [0].x, corners [2].x);
			screenPos.y = Mathf.Clamp (screenPos.y * rt.rect.height + corners [0].y, corners [0].y, corners [1].y);

			screenPos.z = 0;
			mo.icon.transform.position = screenPos;
		}
	}

	// Update is called once per frame
	void Update () {
		DrawIcons ();
	}
}
